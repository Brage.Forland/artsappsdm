
loadCovariates <- function(covariateDir,includeFiles='*.tif', excludeFiles='template.tif') {
  files <- list.files(covariateDir,pattern=glob2rx(includeFiles))
  if(!is.null(excludeFiles)) {
    files <- grep(glob2rx(excludeFiles), files, value=TRUE, invert=TRUE)
  }
  
  files <- paste(covariateDir, files, sep="/")
  rbrick <- raster::brick(lapply(files, raster::raster))
  
  if(file.exists(paste(covariateDir,'layers.txt',sep="/"))) {
    layerDesc <- readr::read_tsv(paste(covariateDir,'layers.txt',sep="/"), col_names=c("layer","description"), col_types="cc")
    description <- as.list(layerDesc$description)
    names(description) <- layerDesc$layer
  } else {
    desc <- NULL
  }
  
  list(raster=rbrick, descriptions=description)
  
}
