
## Todo: only resolve names not already resolved

gbifResolveNames <- function(species, nameCacheFile=NULL, useCache=TRUE, keepUnresolved=FALSE) {
	
	if(!is.null(nameCacheFile) && file.exists(nameCacheFile) && useCache) {
		speciesList <- readr::read_tsv(nameCacheFile, col_types="cccc")
		if(is.null(speciesList$dirName))
			speciesList <- tibble::tibble(speciesName=character(), resolvedName=character(), taxonKey=character(), dirName=character())
	} else {
		speciesList <- tibble::tibble(speciesName=character(), resolvedName=character(), taxonKey=character(), dirName=character())
	}
	
	speciesMissing <- setdiff(species,speciesList$speciesName)

	for(sp in speciesMissing) {
		gbifName <- rgbif::name_backbone(sp, strict=TRUE, rank="SPECIES")
		taxonKey <- NA
    resolvedName <- NA
    canonicalName<- sp
		if(gbifName$matchType == "EXACT") {
			if(gbifName$status=="SYNONYM") {
				taxonKey <- gbifName$acceptedUsageKey
			} else {
				taxonKey <- gbifName$usageKey
			}
      resolvedName <- gbifName$species
      canonicalName <- gbifName$canonicalName
		}

    speciesName <- sp
        
    # We want the dirname to be a valid R variable name
    dirName <- iconv(canonicalName, from="UTF-8", to="ASCII//TRANSLIT")
    dirName <- gsub("-|\\s+","_",dirName)  
    dirName <- make.names(dirName)
    
    speciesList <- tibble::add_row(speciesList,
    																speciesName=as.character(speciesName),
    																resolvedName=as.character(resolvedName),
    																taxonKey=as.character(taxonKey),
    																dirName=as.character(dirName))
  }		

  if(!is.null(nameCacheFile)){
    tryCatch(
      readr::write_tsv(speciesList,nameCacheFile),
      error = function(err) {
        warning(paste("Unable to save species names to:",nameCacheFile, sep = " "))
      })
  }

  if(keepUnresolved == FALSE) {
    speciesList <- speciesList[!is.na(speciesList$taxonKey),]
  }
  
	speciesList
}


